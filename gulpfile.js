var gulp = require('gulp');
var clear = require('del');
var concat = require('gulp-concat');
var connect = require('gulp-connect');
var uglify = require('gulp-uglify');
var minifycss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');

var appFonts = [
  'assets/fonts/**'
];

// Vendor Files
var vendorScripts = [
  'app/vendors/jquery/dist/jquery.min.js',
  'app/vendors/jquery/dist/jquery.min.js',
  'app/vendors/html2canvas/build/html2canvas.min.js',
  'assets/scripts/*.js',
  'app/vendors/recordrtc/RecordRTC.min.js',
  'app/vendors/recordrtc/libs/screenshot.js',
  'app/app.js'

];
var vendorStyles = [
  'assets/styles/*.css'
];

// Start the server
gulp.task('server', ['default'], function() {
  connect.server({
    root: "www",
    port: 2000,
    host: '127.0.0.1',
    livereload: true
  });
});

// Clean
gulp.task('clean', function(cb) {
  clear(['www/scripts', 'www/assets'], cb);
});

// Bower
gulp.task('bower', function() {
  gulp.src('bower/components/**/')
    .pipe(gulp.dest('app/vendors'));
});

// Fonts
gulp.task('fonts', function() {
  gulp.src(appFonts)
    .pipe(gulp.dest('www/assets/fonts/'))
});

// Vendor
gulp.task('vendors', ['bower'], function() {
  gulp.src(vendorScripts)
    .pipe(concat('drj-feedback.js'))
    .pipe(gulp.dest('www/scripts'))
  gulp.src(vendorStyles)
    .pipe(concat('drj-feedback.css'))
    .pipe(gulp.dest('www/assets/styles'))
});

// Views
gulp.task('views', function() {
  gulp.src('examples/freelancer/**/*.*')
    .pipe(gulp.dest('www/examples/freelancer'));
  gulp.src('examples/creative/**/*.*')
    .pipe(gulp.dest('www/examples/creative'));
  gulp.src('examples/ricardo/**/*.*')
    .pipe(gulp.dest('www/examples/ricardo'));
});


// Default task
gulp.task('default', function() {
  // gulp.start('scripts', 'vendors', 'views', 'styles', 'images', 'fonts');
  gulp.start('clean','vendors','fonts', 'views');
});

