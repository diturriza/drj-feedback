// Document Ready
$(document).ready(function () {
    // Offcanvas Left Menu
	$(".toggle-menu").on("click", function() {
		$(".body-container").toggleClass("nav-small")
	});
	
	$( ".class-select" ).parent().css( "width", "20%" );
	
	// Drag & drop
	
		$( "#sortable" ).sortable();
	    $( ".non-sortable" ).disableSelection();
	
		$( "#datepicker" ).datepicker();
		$( "#datepicker2" ).datepicker();
		$( "#datepicker3" ).datepicker();
		$( "#datepicker4" ).datepicker();
		$( "#datepicker5" ).datepicker();
		$( "#datepicker6" ).datepicker();
		$( "#datepicker7" ).datepicker();
		$( "#datepicker8" ).datepicker();
		$( "#datepicker9" ).datepicker();
		$( "#datepicker10" ).datepicker();
		$( "#datepicker11" ).datepicker();
		$( "#datepicker12" ).datepicker();
		$( "#datepicker13" ).datepicker();
		$( "#datepicker14" ).datepicker();

		$("#price_range").ionRangeSlider({
			type: "double",
			min: 20,
			max: 1000,
			from: 20,
			to: 1000,
			step: 20,
			prettify_separator: ",",
		});

		$("#price_range2").ionRangeSlider({
			type: "double",
			min: 20,
			max: 1000,
			from: 20,
			to: 1000,
			step: 20,
			prettify_separator: ",",
		});

		$("#price_range3").ionRangeSlider({
			type: "double",
			min: 0,
			max: 10,
			from: 0,
			to: 10,
			step: 1,
			prettify_separator: ",",
		});
		
		$("#price_range4").ionRangeSlider({
			type: "double",
			min: 20,
			max: 1000,
			from: 20,
			to: 1000,
			step: 20,
			prettify_separator: ",",
		});

		jQuery(window).load(function(){
		  
		  // Resolution NOT  below 768
		  if(jQuery(window).width() <= 768) { 
				$('.footable').footable();
	 	  }
		 
		 //Magic arrow markup
	
    var $el, leftPos, newWidth;
    /* Add Magic Line markup via JavaScript, because it ain't gonna work without */
    $("#magicArrow").append("<li id='magic-line'></li>");
    
    /* Cache it */
    var $magicLine = $("#magic-line");
    
    $magicLine
        .width($(".current_page_item").width())
       // .css("left", $(".current_page_item a").position().left)
        .data("origLeft", $magicLine.position().left)
        .data("origWidth", $magicLine.width());
        
		$("#magicArrow > li.dropdown > a").click(function() {
		 $('.current_page_item').removeClass('current_page_item');
			$(this).parent('li').addClass('current_page_item');
			$el = $(this);
			leftPos = $el.position().left;
			newWidth = $el.parent().width();
			
			$magicLine.stop().animate({
				left: leftPos,
				width: newWidth
			});
		});
	  });
	  
    $('#swapArchived').on('click', function () {
        var $el = $(this),
        textNode = this.lastChild;
        $el.find('span').toggleClass('glyphicon-minus');
        textNode.nodeValue = 'Show ' + ($el.hasClass('showArchieved') ? 'Less' : 'Details')
        $el.toggleClass('showArchieved');
		$("#act1").slideToggle( "slow" );
    });

    $('#swapArchived2').on('click', function () {
        var $el = $(this),
        textNode = this.lastChild;
        $el.find('span').toggleClass('glyphicon-minus');
        textNode.nodeValue = 'Show ' + ($el.hasClass('showArchieved') ? 'Less' : 'Details')
        $el.toggleClass('showArchieved');
		$("#act2").slideToggle( "slow" );
    });  
        
	$('#horizontalTab1').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion           
        width: 'auto', //auto or any width like 600px
        fit: true,   // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });

    $('#horizontalTab2').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion           
        width: 'auto', //auto or any width like 600px
        fit: true,   // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });


	$("select[multiple='multiple']").multiselect();


	$('.list-group-item').click(function() {
		$(this).toggleClass('select-multi');
	});
		
		
	//custom js from sunny
	
	$('.filter-block ul li a').on('click',function(){
		
		//$(this).siblings('span').fadeOut('slow');
		$(this).parent().empty('slow');
		
	});
        
    
    //add row
    $('#act2 .addBtnCont button').on('click',function(){
        if($(this).hasClass('addRowBtn_carrier')){
            var tableName = 'carrierCost';
        }
        else if($(this).hasClass('addRowBtn_customer')){
            var tableName = 'customerCost';
        }
        $("#"+tableName).each(function () {
            var tds = '<tr>';
            jQuery.each($('tr:last td', this), function () {
                tds += '<td>' + $(this).html() + '</td>';
            });
            tds += '</tr>';
            if ($('tbody', this).length > 0) {
                $('tbody', this).append(tds);}
            else {
                $(this).append(tds);
            }
        });
    });
    
    $('.addBtnCont button').on('click',function(){
        if($(this).hasClass('addRowBtn_carrier')){
            //var tableName = 'carrierCost';
            var tableName = $(this).parent().parent().find('table').prop('id');
            //alert(tableName);
        }
        else if($(this).hasClass('addRowBtn_customer')){
            //var tableName = 'customerCost';
            var tableName = $(this).parent().parent().find('table').prop('id');
            //alert(tableName);
        }
        $("#"+tableName).each(function () {
            var tds = '<tr>';
            jQuery.each($('tr:last td', this), function () {
                tds += '<td>' + $(this).html() + '</td>';
            });
            tds += '</tr>';
            if ($('tbody', this).length > 0) {
                $('tbody', this).append(tds);}
            else {
                $(this).append(tds);
            }
        });
    });
    
    //del row
    $('table').on('click', '.deleteRow', function(e){
       $(this).closest('tr').remove();
    });
    
    $('#timepicker1,#timepicker2,#timepicker3,#timepicker4,#timepicker5,#timepicker6').timepicker({
        minuteStep: 5,
        showInputs: false,
        disableFocus: true
    
    });
	 $('.check_calls_inner').slimScroll({
        height: '110px',
		railVisible: true,
    	alwaysVisible: true
    });
	
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
	
	$('.contract_table .table-responsive table').find('input[type="checkbox"]').change(function() {
	  if ($(this).is(':checked')) {
		$( this ).parent().parent().parent().parent('tr').addClass( "selected" );
	  } else {
		$( this ).parent().parent().parent().parent('tr').removeClass( "selected" );
	  }
	});

    $( ".upload_rate p a" ).click(function() {
        $( this ).toggleClass( "off" );
    });
	
	
	
	$('#default_theme').click(function(){
		$('link[rel=stylesheet][href~="css/style-green.css"]').remove();
		$('link[rel=stylesheet][href~="css/style-orange.css"]').remove();
		$('link[rel=stylesheet][href~="css/style-red.css"]').remove();
	});
	$('#green_theme').click(function(){
		$("head").append("<link>");
		var css = $("head").children(":last");
		css.attr({
		      rel:  "stylesheet",
		      type: "text/css",
		      href: "css/style-green.css"
		});
	});
	$('#orange_theme').click(function(){
		$("head").append("<link>");
		var css = $("head").children(":last");
		css.attr({
		      rel:  "stylesheet",
		      type: "text/css",
		      href: "css/style-orange.css"
		});
	});
	$('#red_theme').click(function(){
		$("head").append("<link>");
		var css = $("head").children(":last");
		css.attr({
		      rel:  "stylesheet",
		      type: "text/css",
		      href: "css/style-red.css"
		});
	});
	
	$("ul.nav-tabs a").click(function (e) {
	  	e.preventDefault();  
		$(this).tab('show');
	});
	
});

( function( $ ) {
  fakewaffle.responsiveTabs( [ 'xs', 'sm' ] );
} )( jQuery );

$('.class-select .dropdown-menu li').click(function(e){
  e.preventDefault();
  var selected = $(this).text();
  $('.category').val(selected);  
});


//$( ".hover" ).mouseover(function() {
//  $( ".ship_cost" ).show();
//});

//$('.hover_div').mouseover(function(event){
//   $(this).find('.ship_cost').show();
//});
//
//$('.hover_div').mouseout(function(event){
//   $(this).find('.ship_cost').hide();
//});

