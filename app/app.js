var cont = 0;

function detectBrowser() {
	 if (navigator.userAgent.indexOf("Chrome") != -1 ) {
		//alert('Chrome');
		return true;
	}
	else {
		//alert('unknown');
		return false;
	}
}

$(document).ready(function(){
	btnLoad();
});

function capture() {

	var h = $(window).height();

	html2canvas($('body'), {
		onrendered: function (canvas) {
			//Set hidden field's value to image data (base-64 string)
			url = canvas.toDataURL("image/png");
			$('#img_val').val(url);
			$('#screen').attr("src",url);
		}
	, height: h});
}

function submit() {

	var formData = JSON.stringify($("#myForm").serializeArray());
	console.log(formData);

	$.ajax({
		type: "POST",
		url: "save.php",
		data: formData,
		dataType: "json",
		contentType : "application/json",
		processData: false,
		success : function(callback) {
						//Where $(this) => context == FORM
						console.log(JSON.parse(callback));
						alert("SUCCESS");
				},
				error : function(){
						alert("Error!");
			}
	});

}

function btnLoad() {

	var btn = '<button id="btn-feedback" data-html2canvas-ignore="true" class="btn btn-default feedback-bottom-right" ></button>';
	$("body").append(btn);
	$("#btn-feedback").hide();

	var btnPhoto = '<button id="btn-photo" data-html2canvas-ignore="true" class="btn btn-primary feedback-bottom-right-1"><span class="glyphicon glyphicon-camera btn-icon" aria-hidden="true"></span> photo </button>';
	$("body").append(btnPhoto);
	var btnVideo = '<button id="btn-video" data-html2canvas-ignore="true" class="btn btn-info feedback-bottom-right-2" ><span class="glyphicon glyphicon-film btn-icon" aria-hidden="true"></span> Video </button>';
	$("body").append(btnVideo);

	$("#btn-photo").hide();
	$("#btn-video").hide();


	$("#btn-feedback").click(function() {
			if ($(this).text() == '<< Back') {
			$(this).html("<< Hide");
				$("#btn-photo").show();
			$("#btn-video").show();
			$("#start").hide();
			$("#stop").hide();
		}
		else {
				//$(this).html("Send Feedback");
				$(this).hide();
				$("#btn-photo").hide();
			$("#btn-video").hide();
			$("#start").hide();
			$("#stop").hide();
			}
	});

/*
	$("#btn-feedback").click(function() {
		if ($(this).text() == '<< Hide') {
			$("#btn-feedback").hide();
			$("#start").hide();
			$("#stop").hide();
		}
	});
*/
	$("#btn-recorder").click(function() {
		$("#btn-feedback").html("<< Hide");
		$("#btn-feedback").show();
		$("#btn-photo").show();
		$("#btn-video").show();
	});

		var recorOpts = '<button id="start" contenteditable="false" class="btn btn-primary feedback-bottom-right-1" data-html2canvas-ignore="true" >Start Recording</button>';
		recorOpts += '<button id="stop" disabled contenteditable="false" class="btn btn-primary feedback-bottom-right-2" data-html2canvas-ignore="true" >Stop</button>';
	$("body").append(recorOpts);

	$("#start").hide();
	$("#stop").hide();

	// CLick Video button Event
	$("#btn-video").click(function() {
		if (cont > 0) {
			alert("Please, refresh the page to record another video");
		}
		else {
			if ($("#btn-feedback").text() == '<< Hide') {
					$(this).hide();
					$('#btn-photo').hide();
					$("#btn-feedback").html("<< Back");
					$("#start").show();
				$("#stop").show();
				recordVideo();
				}
		}

	});

	var dialog = '<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\
			<div class="modal-dialog-lg">\
				<div class="modal-content">\
					<div class="modal-header feedback-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button><h3 id="myModalLabel">Send Feedback</h3></div>\
						<div id="myModal-body" class="modal-body">\
							<div class="container-fluid">\
										<div class="row">\
											<form method="POST" enctype="multipart/form-data" action="save.php" id="myForm">\
												<div id="preview-box" class="col-md-6"></div>\
													<div class="col-md-6">\
														<label for="issue">Please describe the issue you are experiencing:</label>\
														<textarea name="issue" id="issue" class="form-control" rows="3"></textarea>\
													</div>\
												</form>\
										</div>\
							</div>\
						</div>\
					<div id="myModal-footer" class="modal-footer"><button type="button" class="btn btn-default " data-dismiss="modal" aria-hidden="true">Close</button>&nbsp;&nbsp;<button type="button" class="btn btn-primary" aria-hidden="true" >Send</button></div>\
				</div>\
			</div>\
		</div>';
	$("body").append(dialog);

	$("#btn-photo").click(function(){
		takePhoto();
	});
}


function takePhoto() {
	capture();
	var imageForm = '<input type="hidden" name="img_val" id="img_val" value="" />\
					 <img id="screen" class="img-responsive img-thumbnail" />';
	$('#preview-box').html(imageForm);
	// var btnSend = '<button type="button" class="btn btn-default " data-dismiss="modal" aria-hidden="true">Close</button>&nbsp;&nbsp;<button type="button" class="btn btn-primary" aria-hidden="true" onclick="submit();">Send</button>';
	// $('#myModal-footer').html(btnSend)
	 setTimeout(function () {
		$('#myModal').modal('show');
	}, 2000);
}

function recordVideo() {

	var h = $(window).height();
	var w = $(window).width();

	console.log(w+"x"+h);

	var elementToShare = $('body');
	// elementToShare.css('height', h);
	var recorder = RecordRTC(elementToShare, {
		recorderType: CanvasRecorder,
		video: { width: 720, height: 480 },
			canvas: { width: 720, height: 480 },
			width: 720,
		height: 480
	});

	$('#start').click(function() {

		var flag = detectBrowser();
		if (flag) {
			recorder.startRecording();
			$('#start').prop( "disabled", true );
			$('#btn-feedback').prop( "disabled", true );
			setTimeout(function() {
				$('#stop').prop( "disabled", false );
			}, 1000);
		}
		else {
			alert("Sorry, this function only is available on Chrome")
		}
	});

	$('#stop').click(function() {
		this.disabled = true;
		recorder.stopRecording(function(url) {

			var video = $('<video />', {
				id: 'video',
				src: url,
				type: 'video/mp4',
				controls: true
			});
			video.css('width','100%');
			$('#preview-box').html(video);
			$('#myModal').modal('show');
			video.prop('play',true);

			var videoInput =  '<input type="hidden" name="video_val" id="video_val" value="'+url+'" />';
			$('#preview-box').append(videoInput);
		});

		$('#btn-feedback').prop('disabled', false);
		cont += 1;
	});

	window.onbeforeunload = function() {
		$('#start').prop( "disabled", false );
		$('#stop').prop( "disabled", true );
	};

}

function sendData () {

	var issue = $('#issue').text();

	$.ajax({
		type: 'POST',
		url: 'localhost',
		data: {
			data: dataMultimedia,
			issue: issue
		},
		success: function (data) {
			// TODO...
		},
		error: function (data) {
			// TODO...
		}
	});
}